from gadgets import get_gadgets, Addresses
from gadgets.pins.rpi import pins

arguments = {
    'locations': {
        'nowhere': {
            'led one': {
                'type': 'shift register switch',
                'channel': 0,
                'bus': 0,
                'device': 0
            },
            'led two': {
                'type': 'shift register switch',
                'channel': 1,
                'bus': 0,
                'device': 0
            },
            'led three': {
                'type': 'shift register switch',
                'channel': 2,
                'bus': 0,
                'device': 0
            },
            'led four': {
                'type': 'shift register switch',
                'channel': 3,
                'bus': 0,
                'device': 0
            },
            'led five': {
                'type': 'shift register switch',
                'channel': 4,
                'bus': 0,
                'device': 0
            },
            'led six': {
                'type': 'shift register switch',
                'channel': 5,
                'bus': 0,
                'device': 0
            },
            'led seven': {
                'type': 'shift register switch',
                'channel': 6,
                'bus': 0,
                'device': 0
            },
            'led eight': {
                'type': 'shift register switch',
                'channel': 7,
                'bus': 0,
                'device': 0
            },
            'i1': {
                'type': 'input',
                'pin': pins['gpio'][11]
            },
        }
    }
}

addresses = Addresses()
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
