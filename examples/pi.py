from gadgets import get_gadgets, Addresses
from gadgets.pins.rpi import pins

arguments = {
    'locations': {
        'lab': {
            'led one': {
                'type': 'switch',
                'pin': pins['gpio'][11],
            },
            'led two': {
                'type': 'switch',
                'pin': pins['gpio'][13],
            },
            'momentary led': {
                'type': 'switch',
                'pin': pins['gpio'][15],
                'momentary': True,
            }
        }
    }
}

addresses = Addresses()
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
