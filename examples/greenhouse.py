import time
import threading
from gadgets import get_gadgets, Addresses, Gadget
from gadgets.pins.beaglebone import pins

arguments = {
    'locations': {
        'bed one': {
            'pump': {
                'type': 'shift register switch',
                'channel': 2,
                },
            'float_switch': {
                'type': 'input',
                'input_type': 'gpio',
                'pin': pins['gpio'][8][11],
                }
            },
        'bed two': {
            'pump': {
                'type': 'shift register switch',
                'channel': 1,
                },
            'float_switch': {
                'type': 'input',
                'input_type': 'gpio',
                'pin': pins['gpio'][8][14],
                }
            },
        'bed three': {
            'pump': {
                'type': 'shift register switch',
                'channel': 3,
                },
            'float_switch': {
                'type': 'input',
                'input_type': 'gpio',
                'pin': pins['gpio'][8][12],
                }
            },
        'fish tank': {
            'temperature': {
                'type': 'thermometer',
                'uid': '28-0000025ed133'
                }
            },
        'greenhouse': {
            'temperature': {
                'type': 'thermometer',
                'uid': '28-00000479f8c6'
                }
            },
        }
    }


class Siphons(Gadget):
    """
    I use this Siphons Gadget to control the pump and valves in
    my aquaponics greenhouse.
    """
    TOO_COLD = 12
    _float_switches = {
        'bed one float_switch': 120,
        'bed two float_switch': 150,
        'bed three float_switch': 160, 
        }

    def event_received(self, event, message):
        sender = message.get('sender')
        if sender in self._float_switches and self._float_switch_is_open(sender, message):
            location = sender.replace(' float_switch', '')
            self.sockets.send('turn off {0} pump'.format(location))
            if not self._too_cold:
                self._start_timer(location)
        elif sender == 'greenhouse temperature':
            self._check_temperature(message)

    def _check_temperature(self, message):
        t = self._get_greenhouse_temperature(message)
        if t < self.TOO_COLD and self._too_cold is False:
            self._too_cold = True
        elif t >= self.TOO_COLD and self._too_cold is True:
            self.sockets.send('turn on bed one pump')
            self.sockets.send('turn on bed two pump')
            self.sockets.send('turn on bed three pump')
            self._too_cold = False
                        
    def _get_greenhouse_temperature(self, message):
        return message.get('locations', {}).get('greenhouse', {}).get('input', {}).get('temperature', {}).get('value')

    def _float_switch_is_open(self, sender, message):
        location = sender.replace(' float_switch', '')
        return not message.get('locations', {}).get(location, {}).get('input', {}).get('float_switch', {}).get('value')
            
    @property
    def events(self):
        return ['UPDATE']

    def on_start(self):
        self._too_cold = False
        self.sockets.send('turn on bed one pump')
        self.sockets.send('turn on bed two pump')
        self.sockets.send('turn on bed three pump')

    def _start_timer(self, location):
        countdown = self._float_switches['{0} float_switch'.format(location)]
        t = threading.Thread(target=self._float_switch_finished, args=(location, countdown))
        t.start()

    def _float_switch_finished(self, location, countdown):
        time.sleep(countdown)
        s = self._get_sockets()
        msg = 'turn on {0} pump'.format(location)
        s.send(msg)
        time.sleep(0.5)
        s.close()

    def _register(self):
        pass
            
if __name__ == '__main__':
    addresses = Addresses()
    siphons = Siphons('na', 'siphons monitor', addresses)
    gadgets = get_gadgets(arguments, addresses)
    gadgets.add_gadget(siphons)
    gadgets.start()
