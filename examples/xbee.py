from gadgets import get_gadgets, Addresses
from gadgets.pins.beaglebone import pins

arguments = {
    'locations': {
        'back yard': {
            'light': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 3,
                'device': '/dev/ttyUSB0'
            },
            'gnome': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 2,
                'device': '/dev/ttyUSB0'
            }
        }
    }
}

addresses = Addresses(in_port=6113, out_port=6114)
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
#                'address':  b'\x00\x13\xa2\x00\x40\x98\x25\xC1',