from gadgets import get_gadgets, Addresses
from gadgets.pins.beaglebone import pins

arguments = {
    'locations': {
        'living room': {
            'light': {
                'type': 'shift register switch',
                'channel': 0,
            },
            'fan': {
                'type': 'shift register switch',
                'channel': 1,
            },
            'night light': {
                'type': 'shift register switch',
                'channel': 8,
            },
            'fly swatter': {
                'type': 'shift register switch',
                'channel': 9,
            },
        },
        'back yard': {
            'missle': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 3,
                'device': '/dev/ttyUSB0'
            },
            'gnome': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 2,
                'device': '/dev/ttyUSB0'
            }
        }
    }
}

addresses = Addresses()
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
