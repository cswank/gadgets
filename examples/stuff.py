from gadgets import get_gadgets, Addresses
from gadgets.pins.beaglebone import pins

arguments = {
    'locations': {
        'living room': {
            'led one': {
                'type': 'shift register switch',
                'channel': 0,
            },
            'led two': {
                'type': 'shift register switch',
                'channel': 1,
            },
            'led three': {
                'type': 'shift register switch',
                'channel': 8,
            },
            'momentary led': {
                'type': 'shift register switch',
                'channel': 9,
                'momentary': True,
            },
            'motor': {
                'type': 'motor',
                'gpio_a': pins['gpio'][8][24],
                'gpio_b': pins['gpio'][8][25],
                'pwm': pins['pwm'][8][13],
                'poller': pins['gpio'][8][27]
            },
            'input': {
                'type': 'button',
                'pin': pins['gpio'][8][30]
            },
        },
        'back yard': {
            'led one': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 3,
                'device': '/dev/ttyUSB0'
            },
            'led two': {
                'type': 'xbee',
                'address': 0x0013A200409825C1,
                'channel': 2,
                'device': '/dev/ttyUSB0'
            }
        }
    }
}


addresses = Addresses()
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
