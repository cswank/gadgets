from gadgets import get_gadgets, Addresses
from gadgets.pins.beaglebone_black import pins

arguments = {
    'locations': {
        'here': {
            'led one': {
                'type': 'switch',
                'pin': pins['gpio'][9][16],
            },
        }
    }
}


addresses = Addresses()
gadgets = get_gadgets(arguments, addresses)
gadgets.start()
