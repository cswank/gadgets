from gadgets import GadgetsFactory
from gadgets.pins.beaglebone_black import pins
#NOTE on bbb the 1-wire pin is at p9.22
args = {
    'locations': {
        'brewery': {
            'fan': {
                'type':'switch',
                'pin': pins['gpio'][8][7],
            },
        },
        'hlt': {
            'valve': {
                'type': 'valve',
                'pin': pins['gpio'][8][8],
                'trigger': {
                    'type': 'float',
                    'pin': pins['gpio'][8][9],
                    'volume': 26.5,
                },
            },
            'heater': {
                'type': 'electric heater',
                'pin': pins['pwm'][8][13],
            },
            'thermometer': {
                'type': 'thermometer',
                'uid': '28-000002b01241'
            }
        },
        'tun': {
            'valve': {
                'type': 'valve',
                'pin': pins['gpio'][8][10],
                'trigger': {
                    'type': 'gravity',
                    'source': 'hlt',
                    'tank_radius': 7.5,
                    'valve_radius': 0.1875,
                    'valve_coefficient': 0.43244,
                },
            },
            'thermometer': {
                'type': 'thermometer',
                'uid': '28-0000025ed750'
            }
        },
        'boiler': {
            'valve': {
                'type': 'valve',
                'pin': pins['gpio'][8][11],
                'trigger': {
                    'type': 'timer',
                    'source': 'tun',
                    'drain_time': 4 * 60,
                },
            },
            'heater': {
                'type': 'electric heater',
                'pin': pins['pwm'][8][19],
            },
            'cooler': {
                'type': 'cooler',
                'pin': pins['gpio'][8][12],
            },
            'thermometer': {
                'type': 'thermometer',
                'uid': '28-0000025f0aba',
            }
        },
        'carboy': {
            'valve': {
                'type': 'valve',
                'pin': pins['gpio'][8][14],
                'trigger': {
                    'type': 'user',
                    'source': 'boiler',
                }
            }
            
        }
    }
}

factory = GadgetsFactory(ensure_off=True, startup_script='/opt/gadgets/extras/load-device-tree.sh')
gadgets = factory(args)
gadgets.start()
