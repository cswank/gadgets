import pymongo
import datetime
import threading
from gadgets import Gadgets, Addresses, Gadget


class Recorder(Gadget):
    """
    If one would like to record the goings on of a gadgets system with
    pymongo, start this and record away.

    NOTE:  be sure to configure the Addresses to point to the gadgets system that you wish to record.
    """

    events = ['UPDATE']

    def on_start(self):
        self._client = pymongo.MongoClient()
        self._db = self._client[self._location]

    def event_received(self, event, message):
        sender = message.get('sender', '')
        if 'pump' in sender:
            message['timestamp'] = datetime.datetime.now()
            self._db.updates.insert(message)

    def _register(self):
        pass


    
class Timer(threading.Thread):
    """
    If this timer is still valid after 30 minutes, something
    must be wrong with the system.  Send an email
    """

    def __init__(self, event):
        self.valid = True
        self._event = event
        super(Timer, self).__init__()

    def run(self):
        time.sleep(30 * 60)
        if self.valid:
            """
            send and email
            """
            pass
        

class Alarm(Gadget):
    """
    In an aquaponics greenhouse the beds should fill up and then drain
    every 15 minutes or so.  If something goes wrong I want to know,
    or else fish, plants, or fish and plants will die.  
    """

    events = [
        'bed one float_switch',
        #'bed two float_switch',  #not using bed two right now
        'bed three float_switch',
        ]

    def _start_timer(self, event):
        t = Timer(event)
        t.start()
        self._timers[event] = t
        
    def on_start(self):
        self._timers = {}
        for event in self.events:
            self._start_timer(event)
        

    def event_received(self, event, message):
        t = self._timers.get(event)
        if t:
            t.valid = False
            self._start_timer(event)
            

    def _register(self):
        pass        

    
if __name__ == '__main__':
    location = 'greenhouse'
    addresses = Addresses(host=location) #configures this gadgets system as a remote, instead of the master
    recorder = Recorder(location, 'recorder', addresses)
    gadgets = Gadgets([recorder], addresses)
    gadgets.start()
