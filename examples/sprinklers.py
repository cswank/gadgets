import sys
from gadgets import get_gadgets, Addresses, Sockets
from gadgets.pins.rpi import pins

sprinklers = {
    'locations': {
        'back garden': {
            'sprinklers': {
                'type': 'switch',
                'pin': pins['gpio'][11],
                'on': 'water {location}',
                'off': 'stop watering {location}'
                },
            },
        'front yard': {
            'sprinklers': {
                'type': 'switch',
                'pin': pins['gpio'][13],
                'on': 'water {location}',
                'off': 'stop watering {location}'
                },
            },
        'sidewalk': {
            'sprinklers': {
                'type': 'switch',
                'pin': pins['gpio'][15],
                'on': 'water {location}',
                'off': 'stop watering {location}'
                },
            },
        'back yard': {
            'sprinklers': {
                'type': 'switch',
                'pin': pins['gpio'][16],
                'on': 'water {location}',
                'off': 'stop watering {location}'
                },
            },
        'front garden': {
            'sprinklers': {
                'type': 'switch',
                'pin': pins['gpio'][18],
                'on': 'water {location}',
                'off': 'stop watering {location}'
                },
            },
        }
    }

if __name__ == '__main__':
    addresses = Addresses()
    gadgets = get_gadgets(sprinklers, addresses)
    gadgets.start()

"""
This is the cron script that I use to water my yard.  The 'gadgets' command
accepts a '--command' argument, which starts a Socket, sends the command, and
then exits.

# Minute   Hour   Day of Month       Month          Day of Week        Command    
# (0-59)  (0-23)     (1-31)    (1-12 or Jan-Dec)  (0-6 or Sun-Sat)
0   5   *  *  Wed  gadgets --command 'water front yard for 20 minutes'
21  5   *  *  Wed  gadgets --command 'water back yard for 20 minutes'
0   5   *  *  Sat  gadgets --command 'water front yard for 20 minutes'
21  5   *  *  Sat  gadgets --command 'water back yard for 20 minutes'
0   4   *  *  *    gadgets --command 'water back garden for 5 minutes'
42  9-18 * * * gadgets --command 'water front garden for 2 minutes'
50  9-18 * * * gadgets --command 'water sidewalk for 2 minutes'
0   3 * * * python2 gadgets --command 'water sidewalk for 30 minutes'

NOTE:  I just planted a bunch of plants in the sidewalk zone and the front garden and it is the middle of summer.  I
am watering them for 2 minutes each hour during the day so they don't dry out, not because I'm a water wasting bastard.
"""
