.. Gadgets documentation master file, created by
   sphinx-quickstart on Sat Feb  2 07:22:30 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.txt

Installation
===================================

.. toctree::
   :maxdepth: 2
   
   installation

IO
===================================
Gadgets.io

If you don't need to use the Robot Command Language
and don't need the rest of the functionality that
gadgets provides but only want to turn a pin on
and off, take a look at gadgets.io.

.. toctree::
   :maxdepth: 2
   
   api/io

UI
===================================
Gadgets.ui

Gadgets comes with a built in user interface.  Read about it
here :ref:`ui-doc`.

.. _robot-command-language:

Robot Command Language (RCL)
=============================

.. include:: ../../gadgets/rcl/README.md


API Documentation
==================

.. toctree::
   :maxdepth: 2

   api

Examples
==================

.. toctree::
   :maxdepth: 2

   examples   


Change Log
=============================

.. include:: ../../CHANGES.txt   
