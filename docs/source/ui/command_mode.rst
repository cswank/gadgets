User Interface Command Mode
============================

Let's turn on something.  Type 'c' to enter command mode, which should
now be indicated by the top bar.  

.. image:: images/ui_4.png

Once the ui is in command mode you can use the up and down arrows (or
for emacs folks like myself, the 'n' and 'p' keys) to highlight the
output devices in the device tree.

.. image:: images/ui_5.png

Once the output device you wish to control is highlighed, hit the enter
key to turn it on.

.. image:: images/ui_6.png

To turn it off hit enter again.

You can also send a command with arguments.  For instance, if you wanted
turn on the pump for 5 seconds you can highlight the target device and
hit the 'a' key (a for arguments).  You will be prompted for the
arguments by a pop up window:

.. image:: images/ui_7.png

Hit enter after typing the arguments and the bet two pump will turn on
for 5 seconds, then turn off  (see the docs for Robot Command Language
for an explaination on command arguments).

Hit 'esc' to exit command mode.
