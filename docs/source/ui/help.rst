User Interface Help
====================

All the commands for this ui are based on keyboard bindings.  To get
a list of the available bindings hit the 'h' key (the fact that you can
do this is shown on the top bar):

.. image:: images/ui_2.png
