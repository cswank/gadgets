.. _ui-doc:

User Interface
=================

Gadgets comes with a built in user interface that is based on
ncurses.  

In the examples directory there is an example of the setup for
my beer brewery.  If a gadgets instance is started up by using
that example::

>>> python brewery.py

Then you can use the ui to connect to it and control it by typing
(in another terminal)::

$ gadgets

Now your screen will be filled with the ui:

.. image:: images/ui_1.png

Here are some of the commands that are available:

.. toctree::
   :maxdepth: 2

   help
   command_mode
   method

You can also download the source code for an `iPhone interface <https://bitbucket.org/cswank/iphonegadgets/>`_ and
install it on your phone (if you are a registered Apple Developer).  I'm 
working on getting the iPhone interace released on the app store.

.. image:: images/iphone.png
