User Interface Method
======================

The ui provides a way to create and run simple methods (see :ref:`robot-command-language`).
To get to the method dialog hit the 'm' key (make sure you are not in command mode):

.. image:: images/ui_8.png

Type 'c' to create a method, then type in your method.  When you are done, hit enter twice 
and you will be prompted for a title.  The one shown here will just turn the fan, wait for
5 seconds, then turn the fan off:

.. image:: images/ui_9.png

After you have entered the title, you are back to the main method dialog.  Hit 'r' to run 
a method and you will see a list of the methods you have saved:

.. image:: images/ui_10.png

Type the number of the method you wish to run and off it goes:

.. image:: images/ui_11.png

You can see the method, and which step it is currently on, on the right side of the main
window.  When the method is finished that part of the window goes blank again.

NOTE:  The methods get saved in your home directory in a hidden dir at ~/.gadgets.  The method is stored 
in an sqlite3 database.  The method that you create will only be available from the computer
you used to create the method.
