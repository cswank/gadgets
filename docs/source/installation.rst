Installation
=================

Gadgets depends on zeromq for it communications.  I've found
that the easist way to get zeromq installed on a Beaglebone
is to use Ubuntu as the linux distribution.  Angstrom does
not include zeromq from its opkg system anymore.  Here are
the steps I use to get gadgets installed on my beaglebone.

1.  Install ubuntu

Follow the instructions here:
http://elinux.org/BeagleBoardUbuntu#Quantal_12.10_armhf

Here are the instructions from the relevant section:

.. toctree::
   :maxdepth: 1

   ubuntu

2.  After you have Ubuntu running on your beaglebone and
you have a serial terminal, run the folling on your beaglebone::

    $ sudo apt-get update
    $ sudo apt-get install build-essential python-dev \
        python-setuptools python-pip

3.  You now have a build system installed on the beaglebone that
can build zeromq.  Currently apt-get installs zeromq-2.2, but
gadgets has been developed with zeromq-3.2.  Here is how to
build it (You might want to check to see if there is a more
recent version from http://www.zeromq.org/intro:get-the-software)::

    $ cd /tmp
    $ wget http://download.zeromq.org/zeromq-3.2.2.tar.gz
    $ tar xf zerommq-3.2.2.tar.gz
    $ cd zeromq-3.2.2
    $ ./configure
    $ make
    $ sudo make install

4.  Now install the python bindings to zeromq (once again, you
might want to check for a more updated version)::

    $ cd /tmp
    $ wget http://pypi.python.org/packages/source/p/pyzmq/pyzmq-2.2.0.1.tar.gz
    $ tar xf pyzmq-2.2.0.1.tar.gz
    $ cd pyzmq-2.2.0.1
    $ python setup.py install

5.  Now install gadgets
using pip (which you installed in step 2)::

    $ sudo pip install gadgets

6.  If you plan on using the pwm pins of your Beaglebone you need to
enable the pwm clocks.  Gadgets includes a script that does that for
you (gadgets.uitls.enable_pwm), but it is nice to have it enabled by
default.  Edit the /etc/rc.local file so the it goes from::

    #!/bin/sh -e
    #
    # rc.local
    #
    # This script is executed at the end of each multiuser runlevel.
    # Make sure that the script will "exit 0" on success or any other
    # value on error.
    #
    # In order to enable or disable this script just change the execution
    # bits.
    #
    # By default this script does nothing.

    exit 0

To this::

    #!/bin/sh -e
    #
    # rc.local
    #
    # This script is executed at the end of each multiuser runlevel.
    # Make sure that the script will "exit 0" on success or any other
    # value on error.
    #
    # In order to enable or disable this script just change the execution
    # bits.
    #
    # By default this script does nothing.
    modprobe ti_tscadc 
    enable-pwm
    exit 0


Note:  when you installed gadgets it installed the enable-pwm script
that rc.local will call.

Thats it.  You now have gadgets installed.  See the rest of the docs to see
how to use it.
