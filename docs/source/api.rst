API Documentation
=================

.. toctree::
   :maxdepth: 1

   api/gadget
   api/devices
   api/sensors
   api/sockets   
   api/io
   api/errors
