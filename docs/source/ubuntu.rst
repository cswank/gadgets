Ubuntu Installation
=================

Get the image::

    $ wget http://ynezz.ibawizard.net/beagleboard/quantal/ubuntu-12.10-r3-minimal-armhf-2013-01-12.tar.xz
    
Verify the image with::

    $ md5sum ubuntu-12.10-r3-minimal-armhf-2013-01-12.tar.xz
    $ 27f2d0236cda3386c67bf8d08394227c  ubuntu-12.10-r3-minimal-armhf-2013-01-12.tar.xz
    
Unpack the image::

    $ tar xJf ubuntu-12.10-r3-minimal-armhf-2013-01-12.tar.xz
    $ cd ubuntu-12.10-r3-minimal-armhf-2013-01-12
    
If you don't know the location of your SD card::

    $ sudo ./setup_sdcard.sh --probe-mmc
    
You should see something like::

    $ Are you sure? I don't see [/dev/idontknow], here is what I do see...
    fdisk -l:
    Disk /dev/sda: 500.1 GB, 500107862016 bytes <- x86 Root Drive
    Disk /dev/mmcblk0: 3957 MB, 3957325824 bytes <- MMC/SD card

mount::

    $ /dev/sda1 on / type ext4 (rw,errors=remount-ro,commit=0) <- x86 Root Partition
    
In this example, we can see via mount, /dev/sda1 is the x86 rootfs, therefore /dev/mmcblk0 is the other drive in the system, which is the MMC/SD card that was inserted and should be used by ./setup_sdcard.sh...
Install image::

    $ sudo ./setup_sdcard.sh --mmc /dev/sdX --uboot bone
    
