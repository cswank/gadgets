:mod:`gadgets.errors`
--------------------------------

GadgetsError
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.errors

  .. autoclass:: GadgetsError
     :members:
     :inherited-members:


