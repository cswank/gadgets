:mod:`gadgets.devices`
--------------------------------
The devices are all dependent on an instance of Gadgets being up and running.
For that reason, the example usage for each device shows how to create each
device by creating the proper configuration dictionary and passing it to
the get_gadgets function.


Device
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.device

  .. autoclass:: Device
     :inherited-members:
     :members:

Switch
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.switch.switch

  .. autoclass:: Switch
     :members:

Switch IO Alternatives
~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 1

   switchio

Motor
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.motor.motor

  .. autoclass:: Motor
     :members:     


Heater
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.heater.electric_heater

  .. autoclass:: ElectricHeater
     :members:     


Valve
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.valve.valve

  .. autoclass:: Valve
     :members:     
     

