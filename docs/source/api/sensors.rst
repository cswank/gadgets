:mod:`gadgets.sensors`
--------------------------------


Thermometer
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.sensors.thermometer

  .. autoclass:: Thermometer
     :members:

