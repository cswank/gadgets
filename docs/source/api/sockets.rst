:mod:`gadgets.sockets`
--------------------------------

.. automodule:: gadgets.sockets

Addresses
~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: Addresses
     :members:



Sockets
~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: Sockets
     :special-members:          
     :members:

