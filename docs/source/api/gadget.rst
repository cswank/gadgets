:mod:`gadgets.gadget`
--------------------------------

Gadget
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.gadget

  .. autoclass:: Gadget
     :members:
     :inherited-members:


:mod:`gadgets.coordinator`
--------------------------------

Coordinator
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.coordinator

  .. autoclass:: Coordinator
     :members:
     :inherited-members:
     
