ShiftRegisterSwitch
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.switch.shift_register.io

  .. autoclass:: ShiftRegisterIO
     :members:    

XBee
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.devices.switch.xbee.io

  .. autoclass:: XBeeIO
     :members:    
