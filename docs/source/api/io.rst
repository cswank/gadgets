:mod:`gadgets.io`
--------------------------------
The classes of gadgets.io can be used outside of the
gadgets system.  They don't depend on zeromq and don't
perform any of the gadgets communication.

IO
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.io.io

  .. autoclass:: IO
     :members:
     

Counter
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.io.counter

  .. autoclass:: Counter
     :members:
     :inherited-members:



GPIO
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.io.gpio

  .. autoclass:: GPIO
     :members:
     :inherited-members:


Poller
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.io.poller

  .. autoclass:: Poller
     :members:
     :inherited-members:

PWM
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gadgets.io.pwm

  .. autoclass:: PWM
    :members:
    :inherited-members:
     
    .. attribute:: duty_percent

        Set the duty_percent with a value from 0 to 100.
        The value is written to the sysfs duty_percent interface.
        If the pwm was turned off before this call, the pwm will be
        turned on.

