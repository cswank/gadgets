Sprinklers
=================

Here is the setup I use to water my yard::

    import sys
    from gadgets import get_gadgets, Addresses, Sockets
    from gadgets.pins.rpi import pins

    sprinklers = {
        'locations': {
            'back garden': {
                'sprinklers': {
                    'type': 'switch',
                    'pin': pins['gpio'][11],
                    'on': 'water {location}'
                    'off': 'stop watering {location}'
                    },
                },
            'front yard': {
                'sprinklers': {
                    'type': 'switch',
                    'pin': pins['gpio'][13],
                    'on': 'water {location}'
                    'off': 'stop watering {location}'
                    },
                },
            'sidewalk': {
                'sprinklers': {
                    'type': 'switch',
                    'pin': pins['gpio'][15],
                    'on': 'water {location}'
                    'off': 'stop watering {location}'
                    },
                },
            'back yard': {
                'sprinklers': {
                    'type': 'switch',
                    'pin': pins['gpio'][16],
                    'on': 'water {location}'
                    'off': 'stop watering {location}'
                    },
                },
            'front garden': {
                'sprinklers': {
                    'type': 'switch',
                    'pin': pins['gpio'][18],
                    'on': 'water {location}'
                    'off': 'stop watering {location}'
                    },
                },
            }
        }

    if __name__ == '__main__':
        addresses = Addresses()
        gadgets = get_gadgets(sprinklers, addresses)
        gadgets.start()


This is the cron script that I use to run a watering schedule.  The 'gadgets'
command accepts a '--command' argument, which starts a Sockets instance, sends the command,
and then exits::

    #Minute  Hour  Day  Month  Day of Week Command
        0     5     *     *        Wed     gadgets --command 'water front yard for 20 minutes'
        0     5     *     *        Fri     gadgets --command 'water front yard for 20 minutes'
        25    5     *     *        Wed     gadgets --command 'water back yard for 20 minutes'
        25    5     *     *        Fri     gadgets --command 'water back yard for 20 minutes'
        50    5     *     *        Wed     gadgets --command 'water sidewalk for 20 minutes'
        50    5     *     *        Fri     gadgets --command 'water sidewalk for 20 minutes'
        0     4     *     *        Mon     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Tue     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Wed     gadgets --command 'water front garden for 5 minutes'    
        0     4     *     *        Thu     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Fri     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Sat     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Sun     gadgets --command 'water front garden for 5 minutes'
        0     4     *     *        Mon     gadgets --command 'water back garden for 5 minutes'
        0     4     *     *        Tue     gadgets --command 'water back garden for 5 minutes'
        0     4     *     *        Wed     gadgets --command 'water back garden for 5 minutes'    
        0     4     *     *        Thu     gadgets --command 'water back garden for 5 minutes'
        0     4     *     *        Fri     gadgets --command 'water back garden for 5 minutes'
        0     4     *     *        Sat     gadgets --command 'water back garden for 5 minutes'
        0     4     *     *        Sun     gadgets --command 'water back garden for 5 minutes'
