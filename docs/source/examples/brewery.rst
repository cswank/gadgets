Brewery
=================

Gadgets was orignially created to run my automatic brewery.  The brewery looks
something like this:

.. image:: images/brewery.jpeg
   :width: 300px

The gadgets system is created like this::   

    from gadgets import GadgetsFactory
    from gadgets.pins.beaglebone import pins

    args = {
        'locations': {
            'brewery': {
                'fan': {
                    'type':'switch',
                    'pin': pins['gpio'][8][17],
                },
            },
            'hlt': {
                'valve': {
                    'type': 'valve',
                    'pin': pins['gpio'][8][11],
                    'trigger': {
                        'type': 'float',
                        'pin': pins['gpio'][8][12],
                        'volume': 26.5,
                    },
                },
                'heater': {
                    'type': 'electric heater',
                    'pin': pins['gpio'][8][25],
                },
                'thermometer': {
                    'type': 'thermometer',
                    'uid': '28-000002b01241'
                }
            },
            'tun': {
                'valve': {
                    'type': 'valve',
                    'pin': pins['gpio'][8][14],
                    'trigger': {
                        'type': 'gravity',
                        'source': 'hlt',
                        'tank_radius': 7.5,
                        'valve_radius': 0.1875,
                        'valve_coefficient': 0.43244,
                    },
                },
                'thermometer': {
                    'type': 'thermometer',
                    'uid': '28-0000025ed750'
                }
            },
            'boiler': {
                'valve': {
                    'type': 'valve',
                    'pin': pins['gpio'][8][15],
                    'trigger': {
                        'type': 'timer',
                        'source': 'tun',
                        'drain_time': 4 * 60,
                    },
                },
                'heater': {
                    'type': 'electric heater',
                    'pin': pins['gpio'][8][24],
                },
                'cooler': {
                    'type': 'cooler',
                    'pin': pins['gpio'][8][22],
                },
                'thermometer': {
                    'type': 'thermometer',
                    'uid': '28-0000025f0aba',
                }
            },
            'carboy': {
                'valve': {
                    'type': 'valve',
                    'pin': pins['gpio'][8][16],
                    'trigger': {
                        'type': 'user',
                        'source': 'boiler',
                    }
                }
                
            }
        }
    }

    factory = GadgetsFactory(ensure_off=True)
    gadgets = factory(args)
    gadgets.
    
    
Brewing a batch of beer is done using a RCL method.  The RCL
method is handled by a MethodRunner.  The MethodRunner broadcasts
the commands in the method.  Here is an example method for brewing
beer::

    heat hlt 176.2 F
    wait for heat hlt

The 'heat hlt 176.2 F' command will get intercepted
by the hlt heater.  The heater will turn on the electric
heating coils and heat the hot liquor tank (hlt) to 176.2 F.
Once that temperature is reached it sends out a message like
this::

    completed heat hlt

The second RCL command (wait for heat htl) causes the MethodRunner
to pause until the 'completed heat hlt' command is broadcast.  The
method then continues::

    fill tun 5.0 gallons
    heat tun 151 F                    
    wait for fill tun
    wait for user grains added

Here is another example of a special RCL command
that is used by the MethodRunner.  'Wait for user'
is for steps that require human intervention.  The built
in curses user interface listens for a 'wait for user'
command and then puts a message on the screen telling the
user to press a certain key when the task is complete.  So
in this case the user would hit the key after the grains
have been added to the mash.  The user interface will then send
this message::

    completed grains added
    
And once again, the RCL method will continue::    
    
    fill hlt                          
    heat hlt 185 F                    
    wait for 60 minutes
    
The 'wait for 60 minutes' is another command that is
used by the MethodRunner.  The method runner will
start a timer that will restart the method after
60 minutes::

    fill tun 6.0 gallons              
    wait for 10 minutes               
    wait for user ready to recirculate 
    fill boiler                       
    wait for user recirculated        
    fill boiler 1                     
    heat boiler 190 F                 
    wait for fill boiler              
    fill tun 4.0 gallons              
    wait for fill tun                 
    stop heating hlt                  
    wait for 2 minutes                
    wait for user ready to recirculate
    fill boiler                       
    wait for user recirculated        
    fill boiler                       
    heat boiler 204 F
    wait for 90 minutes
    stop heating boiler
    wait for 10 minutes
    cool boiler 80 F
    wait for 10 minutes
    fill carboy
    wait for user carboy filled
    stop filling carboy
