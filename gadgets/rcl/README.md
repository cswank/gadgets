All Devices in the gadgets system respond to Robot Command Language (RCL).
A RCL command takes the form of a simple string.  It consists of:

    <action> <location> <device name> <command argument> <units>

So, for example::

    turn on living room light
    turn on living room light 30 minutes

Here are the commands for all the built in devices (with optional arguments
shown enclosed in parentheses.

Switch::

    turn on <location> <name> (for <time> <seconds|minutes|hours>)
    turn off <location> <name>
    
Valve::

    fill <location> <name> (<volume> <liters|gallons>)
    stop filling <location> <name>
    
Heater::

    heat <location> <name> (<temperature> <C|F>)
    stop heating <location> <name>
    
Cooler::

    cool <location> <name> (<temperature> <C|F>)
    stop cooling <location> <name>
    
Motor::

    turn on <location> <name> (<number of encoder edges|time> <ticks|(seconds|minutes|hours)>)
    turn off <location> <name>
    

In these examples turn on and turn off are the actions, living room
is the location, and light is the device.  A gadgets system that
contained this device would be created by:

    >>> from gadgets.pins.beaglebone import pins
    >>> from gadgets import get_gadgets
    >>> arguments = {
    ...     "locations": {
    ...         "living room": {
    ...             "light": {
    ...             "type": "switch",
    ...             "pin": pins["gpio"][8][3]
    ...             }
    ...         }
    ...     }
    ... }
    >>> gadgets = get_gadgets(arguments)
    >>> gadgets.start()


Gadgets was created to control my automatic beer brewery.  I wrote
a seperate interface to gadgets that is built for brewing beer that
is called brewi.  Here is a RCL method that brewi generates based
on a beer recipe that it is given::

    heat hlt 176.2 F
    wait for heat hlt                 
    fill tun 5.0 gallons              
    heat tun 151 F                    
    wait for fill tun                 
    wait for user grains added        
    fill hlt                          
    heat hlt 185 F                    
    wait for 60 minutes               
    fill tun 6.0 gallons              
    wait for 10 minutes               
    wait for user ready to recirculate 
    fill boiler                       
    wait for user recirculated        
    fill boiler 1                     
    heat boiler 190 F                 
    wait for fill boiler              
    fill tun 4.0 gallons              
    wait for fill tun                 
    stop heating hlt                  
    wait for 2 minutes                
    wait for user ready to recirculate
    fill boiler                       
    wait for user recirculated        
    fill boiler                       
    heat boiler 204 F
    wait for 90 minutes
    stop heating boiler
    wait for 10 minutes
    cool boiler 80 F
    wait for 10 minutes
    fill carboy
    wait for user carboy filled
    stop filling carboy

The 'wait for user' commands cause the RCL script to pause until a message
is recieved from a user.  For example, the command::

    wait for user ready to recirculate

Will cause the system to wait for this message::

    ready to recirculate

