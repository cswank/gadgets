from gadgets.devices.switch.shift_register.factory import shift_register_switch_factory
from gadgets.devices.switch.gpio.factory import switch_factory
from gadgets.devices.switch.xbee.factory import xbee_factory
