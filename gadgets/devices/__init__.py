from gadgets.devices.cooler.cooler_factory import cooler_factory
from gadgets.devices.heater.electric_heater_factory import electric_heater_factory
from gadgets.devices.motor.motor_factory import motor_factory
from gadgets.devices.switch.gpio.factory import switch_factory
from gadgets.devices.switch.xbee.factory import xbee_factory
from gadgets.devices.valve.valve_factory import ValveFactory
from gadgets.devices.cooler.cooler_factory import cooler_factory

from gadgets.devices.switch.switch import Switch
from gadgets.devices.valve.valve import Valve
from gadgets.devices.heater.electric_heater import ElectricHeater
