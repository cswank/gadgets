from pymongo import Connection
from brewi.method import batch_brew_method, get_method
from nose.tools import eq_


class TestMethod(object):

    def test_get_method(self):
        c = Connection()
        db = c.beer
        recipe = db.recipes.find_one({'title':'ike'})
        method = get_method(recipe, 33.3)
        eq_(method, 'fill hlt 7.07 gallons\nheat hlt 174.54\nwait for heat hlt\nfill tun 5.0\nheat tun 151\nwait for: fill tun\nwait for user: grains added\nfill hlt 7.07 gallons\nheat hlt 185\nwait for 60 minutes\nfill tun 6.0\nwait for 10 minutes\nwait for user: ready to recirculate\nfill boiler\nwait for user: recirculated\nfill boiler 1\nheat boiler 190\nwait for: fill boiler\nfill tun 4.0 gallons\nwait for: fill tun\nstop heating hlt\nwait for 2 minutes\nwait for user: ready to recirculate\nfill boiler\nwait for user: recirculated\nfill boiler 7\nheat boiler 204\nturn on fan\nwait for 60 minutes\nstop heating boiler\nturn off fan\nwait for 5 minutes\ncool boiler 80\nwait for: cool boiler\nwait for user: ball valve open\nfill carboy\nwait for user: boiler empty\nfill fermenter')
