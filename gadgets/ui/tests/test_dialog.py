from brewi.dialogs import Dialog
from nose.tools import eq_

class FakeScreen(object):

    def getmaxyx(self):
        return 90, 50

    def subwin(self):
        pass
    

class TestDialog(object):


    def setup(self):
        self.d = Dialog()
        self.d._width = 60
        self.d._height = 40

    def test_create(self):
        pass

    def test_get_center(self):
        screen = FakeScreen()
        row, col = self.d._get_center(screen)
        eq_(row, 5)
        eq_(col, 15)
