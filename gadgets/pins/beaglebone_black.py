"""
In order to use these gpio pins
it should be neccesary to disable
htmi by editing uEnv.txt:

  # mount /dev/mmcblk0p1  /mnt/card

then edit /mnt/card/uEnv.txt so the
optargs line looks like:

optargs=quiet capemgr.disable_partno=BB-BONELT-HDMI,BB-BONELT-HDMIN

  # umount /mnt/card
  # shutdown -r now
"""

pins = {
    'adc': {
        9: {
            35:{'name': 'ain7'},
            36:{'name': 'ain6'},
            33:{'name': 'ain5'},
            37:{'name': 'ain3'},
            38:{'name': 'ain4'},
            39:{'name': 'ain1'},
            40:{'name': 'ain2'},
        },
    },
    'pwm': {
        8: {
            13:{
                'directory': 'ocp.*/pwm_test_P8_13.*',
                },
            19: {
                'directory': 'ocp.*/pwm_test_P8_19.*',
                },
            },
        9: {
            14: {
                'directory': 'ocp.*/pwm_test_P9_14.*',
            },
            16:{
                'directory': 'ocp.*/pwm_test_P9_16.*',
            },
            21:{
                'directory': 'ocp.*/pwm_test_P9_21.*',
            },
            22:{
                'directory': 'ocp.*/pwm_test_P9_22.*',
            },
        },
    },
    'gpio': {
        8: {
            7: {
                'export':66
            },
            8: {
                'export':67
            },
            9: {
                'export':69
            },
            10: {
                'export':68
            },
            11: {
                'export':45
            },
            12: {
                'export':44
            },
            14: {
                'export':26
            },
            15: {
                'export':47
            },
            16: {
                'export':46
            },
            26:  {
                'export':61
            },
        },
        9: {
            12: {
                'export':60
            },
            14: {
                'export':50
            },
            15: {
                'export':48
            },
            16: {
                'export':51
            },
        }
    },
}
