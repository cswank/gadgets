import os
from nose.tools import eq_
from gadgets.io.gpio import GPIO
from gadgets.pins.beaglebone import pins

def setup():
    GPIO._path = '/tmp/class/gpio'
    paths = [
        ('_export_path', '/tmp/class/gpio/export', '/tmp/class/gpio/export'),
        ('_gpio_path',   '/tmp/class/gpio/gpio{0}', '/tmp/class/gpio/gpio38'),
        ('_base_path',   '/tmp/class/gpio/gpio{0}/{1}', '/tmp/class/gpio/gpio38'),
        ('_mux_path',    '/tmp/kernel/debug/omap_mux', '/tmp/kernel/debug/omap_mux')
    ]
    for name, attr, path in paths:
        if not os.path.exists(path):
            os.makedirs(path)
        setattr(GPIO, name, attr)

class TestGPIO(object):

    def setup(self):
        self.pin = pins['gpio'][8][3]
        self.gpio = GPIO(self.pin, direction='out')

    def teardown(self):
        f = open('/tmp/class/gpio/gpio38/value', 'w')
        f.close()
        f = open('/tmp/kernel/debug/omap_mux/gpmc_ad6', 'w')
        f.close()

    def test_create(self):
        f = open('/tmp/kernel/debug/omap_mux/gpmc_ad6', 'r')
        eq_(f.read(), '7')
        f.close()
        
    def test_on(self):
        self.gpio.on()
        f = open('/tmp/class/gpio/gpio38/value', 'r')
        eq_(f.read(), '01')
        f.close()

    def test_off(self):
        self.gpio.on()
        f = open('/tmp/class/gpio/gpio38/value', 'r')
        eq_(f.read(), '01')
        f.close()
        self.gpio.off()
        f = open('/tmp/class/gpio/gpio38/value', 'r')
        eq_(f.read(), '010')
        f.close()

    def test_get_mux(self):
        mux = self.gpio._get_mux()
        eq_(mux, '7')

    def test_get_input_mux(self):
        self.gpio.close()
        gpio = GPIO(self.pin, direction='in')
        mux = gpio._get_mux()
        eq_(mux, '27')

    def test_get_input_mux_with_pullup(self):
        self.gpio.close()
        gpio = GPIO(self.pin, direction='in', pullup=True)
        mux = gpio._get_mux()
        eq_(mux, '37')
        