import time, threading, random
from nose.tools import eq_
from gadgets import Gadgets, Addresses, Sockets, Broker, get_gadgets
from gadgets.devices.switch import shift_register_switch_factory
from gadgets.devices.switch.shift_register.server import ShiftRegisterServer

port = 0

def get_fake_gpio():
    return FakeGPIO()

class FakeSPI(object):

    def __init__(self, *args, **kw):
        self.addresses = Addresses(in_port=port, out_port=port+1, req_port=port+2)
        self.sockets = Sockets(self.addresses)

    def writebytes(self, value):
        self.sockets.send('fake spi', value)

        
class TestShiftRegisterSwitch(object):

    def setup(self):
        global port
        port = random.randint(5000, 50000)
        self.port = port
        self.channel = 2
        ShiftRegisterServer._SPI_Class = FakeSPI
        self.addresses = Addresses(in_port=port, out_port=port+1, req_port=port+2)
        self.sockets = Sockets(self.addresses, events=['fake spi'])

    def make_gadgets(self):
        arguments = {
            'channel': self.channel
            }
        self.switch = shift_register_switch_factory(
            'living room',
            'light',
            arguments,
            self.addresses,
        )
        self.gadgets = Gadgets([self.switch], self.addresses)

    def teardown(self):
        self.sockets.send('shutdown')
        shift_register_switch_factory.server = None
        self.sockets.close()

    # def test_create(self):
    #     self.make_gadgets()
    #     t = threading.Thread(target=self.gadgets.start)
    #     t.start()
    #     time.sleep(0.5)



    def test_with_gadgets_factory(self):
        arguments = {
            'locations': {
                'back yard': {
                    'sprinklers': {
                        'type': 'shift register switch',
                        'channel': 3,
                        'on': 'water {location}',
                        'off': 'stop watering {location}'
                        }
                    }
                }
            }
        gadgets = get_gadgets(arguments, self.addresses)
        t = threading.Thread(target=gadgets.start)
        t.start()
        event, message = self.sockets.recv()
        time.sleep(1)
        self.sockets.send('water back yard')
        event, message = self.sockets.recv()
        eq_(message, [2**3])
        self.sockets.send('stop watering back yard')
        event, message = self.sockets.recv()
        eq_(message, [0])

    def test_on_and_off(self):
        self.make_gadgets()
        t = threading.Thread(target=self.gadgets.start)
        t.start()
        event, message = self.sockets.recv()
        eq_(message, [0])
        time.sleep(1)
        
        self.sockets.send('turn on living room light')
        event, message = self.sockets.recv()
        eq_(message, [4])
        self.sockets.send('turn off living room light')
        event, message = self.sockets.recv()
        eq_(message, [0])
        
    def test_on_and_off_with_time(self):
        self.make_gadgets()
        t = threading.Thread(target=self.gadgets.start)
        t.start()
        event, message = self.sockets.recv()
        eq_(message, [0])
        time.sleep(1)
        
        self.sockets.send('turn on living room light for 0.5 seconds')
        time.sleep(1)
        event, message = self.sockets.recv()
        eq_(message, [4])
        event, message = self.sockets.recv()
        eq_(message, [0])
