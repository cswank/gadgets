import os, time, random, threading
from nose.tools import eq_
from gadgets.io.gpio import GPIO
from gadgets.pins.beaglebone import pins
from gadgets import Addresses, Sockets, Gadgets
from gadgets.devices.input.factory import input_factory
from gadgets.devices.input.adc import ADCPoller



def setup():
    ADCPoller.InputClass = FakeADC
    

class FakeADC(threading.Thread):

    def __init__(self, target, pin, threshold=2048, sleep_time=1, delay=None):
        self._target = target
        super(FakeADC, self).__init__()

    @property
    def value(self):
        return False

    def run(self):
        time.sleep(2)
        self._target(2046)

    def close(self):
        pass
    

class TestInputACD(object):

    def setup(self):
        port = random.randint(5000, 50000)
        self.addresses = Addresses(in_port=port, out_port=port+1, req_port=port+2)
        pin = None
        self.sockets = Sockets(self.addresses, events=['update'])
        self.input = input_factory('left', 'input', {'pin':None, 'input_type': 'adc'}, self.addresses)
        self.gadgets = Gadgets([self.input], self.addresses)

    def test_create(self):
        pass

    def test_run(self):
        t = threading.Thread(target=self.gadgets.start)
        t.start()
        time.sleep(1)
        event, message = self.sockets.recv()
        eq_(event, 'update')
        expected = {u'left': {u'input': {u'input': {u'value': 2046}}}}
        eq_(message, expected)
        self.sockets.send('shutdown')
